﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2024 v5.8.242
	 Created on:   	23.05.2024 16:33
	 Created by:   	thinder
	 Organization: 	GWDG
	 Filename:     	FindPerson_by_EMail_via_IDM.ps1
	===========================================================================
	.DESCRIPTION
		Abruf von Personen-Informationen mittels der IDM-Portal REST API.
#>

$username = Read-Host "User name"
$password = Read-Host "Password"
$secpassword = ConvertTo-SecureString $password -AsPlainText -Force
$pscred = New-Object System.Management.Automation.PSCredential($username, $secpassword)
$personEMail = Read-Host "E-Mail"
$findPersonbyEMailviaIDM = "https://idm.gwdg.de/api/v3/Benutzerverwaltung/objects?filter=(%24goesternProxyAddresses -eq '" + $personEMail + "')"

$headers = @{
	'Host' = 'idm.gwdg.de'
	'Accept' = 'application/json'
	'Accept-Language' = 'de-DE'
}

Invoke-RestMethod -Uri $findPersonbyEMailviaIDM -Method Get -Headers $headers -Credential $pscred


