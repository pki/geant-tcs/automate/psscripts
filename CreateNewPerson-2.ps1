﻿<#
	.SYNOPSIS
		A brief description of the CreateNewPerson.ps1 file.
	
	.DESCRIPTION
		Create a new user in a department.
	
	.PARAMETER setValidationTypeHigh
		Set validationType fpr the created Person to high.
	
	.NOTES
		===========================================================================
		Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2022 v5.8.198
		Created on:   	11.02.2022 15:09
		Created by:   	thinder
		Organization: 	GWDG
		Filename:     	CreateNewPerson.ps1
		===========================================================================
#>
param
(
	[Parameter(Position = 0)]
	[switch]$setValidationTypeHigh
)

$username = Read-Host "IDM-PORTAL REST API User-specific credentials name"
$password = Read-Host "IDM-PORTAL REST API User-specific credentials Password"
$secpassword = ConvertTo-SecureString $password -AsPlainText -Force
$pscred = New-Object System.Management.Automation.PSCredential($username, $secpassword)
$personEMail = Read-Host "E-Mail"
$findPersonbyEMailviaIDM = "https://idm.gwdg.de/api/v3/Benutzerverwaltung/objects?filter=(%24goesternProxyAddresses -eq '" + $personEMail + "')"

$headers = @{
	'Host'		      = 'idm.gwdg.de'
	'Accept'		  = 'application/json'
	'Accept-Language' = 'de-DE'
}

$result = Invoke-RestMethod -Uri $findPersonbyEMailviaIDM -Method Get -Headers $headers -Credential $pscred


$cred = Get-Credential
$createNewPersonUrl = "https://cert-manager.com/api/person/v1"
$firstName = ($result.Objects.attributes | Out-ConsoleGridView -Title "givenName" -Filter "givenName").value
$lastName = ($result.Objects.attributes | Out-ConsoleGridView -Title "sn" -Filter "sn").value
$eMail = ($result.Objects.attributes | Out-ConsoleGridView -Title "EMail" -Filter "Primäre").value
$orgId = Read-Host "Organization Id"
$altEmail = ($result.Objects.attributes | Out-ConsoleGridView -Title "EMail" -Filter "E-Mail-Adressen").value
$commonName = $firstName + " " + $lastName
$validationType = "HIGH"

if ($setValidationTypeHigh)
{
	$validationType = "HIGH"
}
else
{
	$validationType = "STANDARD"
}

$headers = @{
	'Content-Type' = 'application/json;charset=utf-8'
	'login'	       = $cred.UserName
	'password'	   = ConvertFrom-SecureString -SecureString $cred.Password -AsPlainText
	'customerUri'  = 'DFN'
}

$body = '{"firstName":"' + $firstName + '","middleName":"","lastName":"' + $lastName + '","email":"' + $eMail + '","organizationId":' + $orgId + ',"validationType":"' + $validationType + '","phone":"","secondaryEmails":["' + $altEmail + '"],"commonName":"' + $commonName + '","eppn":"","upn":null}'

# der Aufruf muss mit -ContentType "application/json; charset=utf-8" erfolgen, damit der JSON Body in UTF-8 codiert wird und bleibt. Damit werden dann auch Sonderzeichen akzeptiert wie z.B. bei folgendem Namen: Téstén
#Invoke-RestMethod -Uri $createNewPersonUrl -Method Post -Headers $headers -Body $body -ContentType "application/json; charset=utf-8"

