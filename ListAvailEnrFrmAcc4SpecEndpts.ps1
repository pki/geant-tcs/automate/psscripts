﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2023 v5.8.219
	 Created on:   	31.03.2023 15:52
	 Created by:   	thinder
	 Organization: 	GWDG
	 Filename:     	ListAvailEnrFrmAcc4SpecEndpts.ps1
	===========================================================================
	.DESCRIPTION
		List available Enrollment Form Accounts for specified Endpoint.
#>

$cred = Get-Credential
$personId = Read-Host "Person Id"
$endpointId = Read-Host "Client Certificate Enrollment Form Endpoint ID"
$listAvailEnrFrmEndptsUrl = "https://cert-manager.com/api/person/v2/" + $personId + "/invitation/endpoint/" + $endpointId + "/account"

$headers = @{
	'login'	       = $cred.UserName
	'password'	   = ConvertFrom-SecureString -SecureString $cred.Password -AsPlainText
	'customerUri'  = 'DFN'
}


Invoke-RestMethod -Uri $listAvailEnrFrmEndptsUrl -Method Get -Headers $headers



# SIG # Begin signature block
# MIIxGQYJKoZIhvcNAQcCoIIxCjCCMQYCAQExDzANBglghkgBZQMEAgEFADB5Bgor
# BgEEAYI3AgEEoGswaTA0BgorBgEEAYI3AgEeMCYCAwEAAAQQH8w7YFlLCE63JNLG
# KX7zUQIBAAIBAAIBAAIBAAIBADAxMA0GCWCGSAFlAwQCAQUABCDyEkr8zU4RRzpN
# O258jxuoaJkSw/XpBxmjxHw22SY8NKCCKicwggQyMIIDGqADAgECAgEBMA0GCSqG
# SIb3DQEBBQUAMHsxCzAJBgNVBAYTAkdCMRswGQYDVQQIDBJHcmVhdGVyIE1hbmNo
# ZXN0ZXIxEDAOBgNVBAcMB1NhbGZvcmQxGjAYBgNVBAoMEUNvbW9kbyBDQSBMaW1p
# dGVkMSEwHwYDVQQDDBhBQUEgQ2VydGlmaWNhdGUgU2VydmljZXMwHhcNMDQwMTAx
# MDAwMDAwWhcNMjgxMjMxMjM1OTU5WjB7MQswCQYDVQQGEwJHQjEbMBkGA1UECAwS
# R3JlYXRlciBNYW5jaGVzdGVyMRAwDgYDVQQHDAdTYWxmb3JkMRowGAYDVQQKDBFD
# b21vZG8gQ0EgTGltaXRlZDEhMB8GA1UEAwwYQUFBIENlcnRpZmljYXRlIFNlcnZp
# Y2VzMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvkCd9G7h6naHHE1F
# RI6+RsiDBp3BKv4YH47kAvrzq11QihYxC5oG0MVwIs1JLVRjzLZuaEYLU+rLTCTA
# vHJO6vEVrvRUmhIKw3qyM2Di2olV8yJY897cz++DhqKMlE+faPKYkEaEJ8d2v+PM
# NSyLXgdkZYLASLCokflhn3YgUKiRx2a163hiA1bwihoT6jGjHqCZ/Tj29icyWG8H
# 9Wu4+xQrr7eqzNZjX3OM2gWZqDioyxd4NlGs6Z70eDqNzw/ZQuKYDKsvnw4B3u+f
# mUnxLd+sdE0bmLVHxeUp0fmQGMdinL6DxyZ7Poolx8DdneY1aBAgnY/Y3tLDhJwN
# XugvyQIDAQABo4HAMIG9MB0GA1UdDgQWBBSgEQojPpbxB+zirynvgqV/0DCktDAO
# BgNVHQ8BAf8EBAMCAQYwDwYDVR0TAQH/BAUwAwEB/zB7BgNVHR8EdDByMDigNqA0
# hjJodHRwOi8vY3JsLmNvbW9kb2NhLmNvbS9BQUFDZXJ0aWZpY2F0ZVNlcnZpY2Vz
# LmNybDA2oDSgMoYwaHR0cDovL2NybC5jb21vZG8ubmV0L0FBQUNlcnRpZmljYXRl
# U2VydmljZXMuY3JsMA0GCSqGSIb3DQEBBQUAA4IBAQAIVvwC8Jvo/6T61nvGRIDO
# T8TF9gBYzKa2vBRJaAR26ObuXewCD2DWjVAYTyZOAePmsKXuv7x0VEG//fwSuMdP
# WvSJYAV/YLcFSvP28cK/xLl0hrYtfWvM0vNG3S/G4GrDwzQDLH2W3VrCDqcKmcEF
# i6sML/NcOs9sN1UJh95TQGxY7/y2q2VuBPYb3DzgWhXGntnxWUgwIWUDbOzpIXPs
# mwOh4DetoBUYj/q6As6nLKkQEyzU5QgmqyKXYPiQXnTUoppTvfKpaOCibsLXbLGj
# D56/62jnVvKu8uMrODoJgbVrhde+Le0/GreyY+L1YiyC1GoAQVDxOYOflek2lphu
# MIIFgTCCBGmgAwIBAgIQOXJEOvkit1HX02wQ3TE1lTANBgkqhkiG9w0BAQwFADB7
# MQswCQYDVQQGEwJHQjEbMBkGA1UECAwSR3JlYXRlciBNYW5jaGVzdGVyMRAwDgYD
# VQQHDAdTYWxmb3JkMRowGAYDVQQKDBFDb21vZG8gQ0EgTGltaXRlZDEhMB8GA1UE
# AwwYQUFBIENlcnRpZmljYXRlIFNlcnZpY2VzMB4XDTE5MDMxMjAwMDAwMFoXDTI4
# MTIzMTIzNTk1OVowgYgxCzAJBgNVBAYTAlVTMRMwEQYDVQQIEwpOZXcgSmVyc2V5
# MRQwEgYDVQQHEwtKZXJzZXkgQ2l0eTEeMBwGA1UEChMVVGhlIFVTRVJUUlVTVCBO
# ZXR3b3JrMS4wLAYDVQQDEyVVU0VSVHJ1c3QgUlNBIENlcnRpZmljYXRpb24gQXV0
# aG9yaXR5MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAgBJlFzYOw9sI
# s9CsVw127c0n00ytUINh4qogTQktZAnczomfzD2p7PbPwdzx07HWezcoEStH2jnG
# vDoZtF+mvX2do2NCtnbyqTsrkfjib9DsFiCQCT7i6HTJGLSR1GJk23+jBvGIGGqQ
# Ijy8/hPwhxR79uQfjtTkUcYRZ0YIUcuGFFQ/vDP+fmyc/xadGL1RjjWmp2bIcmfb
# IWax1Jt4A8BQOujM8Ny8nkz+rwWWNR9XWrf/zvk9tyy29lTdyOcSOk2uTIq3XJq0
# tyA9yn8iNK5+O2hmAUTnAU5GU5szYPeUvlM3kHND8zLDU+/bqv50TmnHa4xgk97E
# xwzf4TKuzJM7UXiVZ4vuPVb+DNBpDxsP8yUmazNt925H+nND5X4OpWaxKXwyhGNV
# icQNwZNUMBkTrNN9N6frXTpsNVzbQdcS2qlJC9/YgIoJk2KOtWbPJYjNhLixP6Q5
# D9kCnusSTJV882sFqV4Wg8y4Z+LoE53MW4LTTLPtW//e5XOsIzstAL81VXQJSdhJ
# WBp/kjbmUZIO8yZ9HE0XvMnsQybQv0FfQKlERPSZ51eHnlAfV1SoPv10Yy+xUGUJ
# 5lhCLkMaTLTwJUdZ+gQek9QmRkpQgbLevni3/GcV4clXhB4PY9bpYrrWX1Uu6lzG
# KAgEJTm4Diup8kyXHAc/DVL17e8vgg8CAwEAAaOB8jCB7zAfBgNVHSMEGDAWgBSg
# EQojPpbxB+zirynvgqV/0DCktDAdBgNVHQ4EFgQUU3m/WqorSs9UgOHYm8Cd8rID
# ZsswDgYDVR0PAQH/BAQDAgGGMA8GA1UdEwEB/wQFMAMBAf8wEQYDVR0gBAowCDAG
# BgRVHSAAMEMGA1UdHwQ8MDowOKA2oDSGMmh0dHA6Ly9jcmwuY29tb2RvY2EuY29t
# L0FBQUNlcnRpZmljYXRlU2VydmljZXMuY3JsMDQGCCsGAQUFBwEBBCgwJjAkBggr
# BgEFBQcwAYYYaHR0cDovL29jc3AuY29tb2RvY2EuY29tMA0GCSqGSIb3DQEBDAUA
# A4IBAQAYh1HcdCE9nIrgJ7cz0C7M7PDmy14R3iJvm3WOnnL+5Nb+qh+cli3vA0p+
# rvSNb3I8QzvAP+u431yqqcau8vzY7qN7Q/aGNnwU4M309z/+3ri0ivCRlv79Q2R+
# /czSAaF9ffgZGclCKxO/WIu6pKJmBHaIkU4MiRTOok3JMrO66BQavHHxW/BBC5gA
# CiIDEOUMsfnNkjcZ7Tvx5Dq2+UUTJnWvu6rvP3t3O9LEApE9GQDTF1w52z97GA1F
# zZOFli9d31kWTz9RvdVFGD/tSo7oBmF0Ixa1DVBzJ0RHfxBdiSprhTEUxOipakyA
# vGp4z7h/jnZymQyd/teRCBaho1+VMIIFgzCCA2ugAwIBAgIORea7A4Mzw4VlSOb/
# RVEwDQYJKoZIhvcNAQEMBQAwTDEgMB4GA1UECxMXR2xvYmFsU2lnbiBSb290IENB
# IC0gUjYxEzARBgNVBAoTCkdsb2JhbFNpZ24xEzARBgNVBAMTCkdsb2JhbFNpZ24w
# HhcNMTQxMjEwMDAwMDAwWhcNMzQxMjEwMDAwMDAwWjBMMSAwHgYDVQQLExdHbG9i
# YWxTaWduIFJvb3QgQ0EgLSBSNjETMBEGA1UEChMKR2xvYmFsU2lnbjETMBEGA1UE
# AxMKR2xvYmFsU2lnbjCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBAJUH
# 6HPKZvnsFMp7PPcNCPG0RQssgrRIxutbPK6DuEGSMxSkb3/pKszGsIhrxbaJ0cay
# /xTOURQh7ErdG1rG1ofuTToVBu1kZguSgMpE3nOUTvOniX9PeGMIyBJQbUJmL025
# eShNUhqKGoC3GYEOfsSKvGRMIRxDaNc9PIrFsmbVkJq3MQbFvuJtMgamHvm566qj
# uL++gmNQ0PAYid/kD3n16qIfKtJwLnvnvJO7bVPiSHyMEAc4/2ayd2F+4OqMPKq0
# pPbzlUoSB239jLKJz9CgYXfIWHSw1CM69106yqLbnQneXUQtkPGBzVeS+n68UARj
# NN9rkxi+azayOeSsJDa38O+2HBNXk7besvjihbdzorg1qkXy4J02oW9UivFyVm4u
# iMVRQkQVlO6jxTiWm05OWgtH8wY2SXcwvHE35absIQh1/OZhFj931dmRl4QKbNQC
# TXTAFO39OfuD8l4UoQSwC+n+7o/hbguyCLNhZglqsQY6ZZZZwPA1/cnaKI0aEYdw
# gQqomnUdnjqGBQCe24DWJfncBZ4nWUx2OVvq+aWh2IMP0f/fMBH5hc8zSPXKbWQU
# LHpYT9NLCEnFlWQaYw55PfWzjMpYrZxCRXluDocZXFSxZba/jJvcE+kNb7gu3Gdu
# yYsRtYQUigAZcIN5kZeR1BonvzceMgfYFGM8KEyvAgMBAAGjYzBhMA4GA1UdDwEB
# /wQEAwIBBjAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBSubAWjkxPioufi1xzW
# x/B/yGdToDAfBgNVHSMEGDAWgBSubAWjkxPioufi1xzWx/B/yGdToDANBgkqhkiG
# 9w0BAQwFAAOCAgEAgyXt6NH9lVLNnsAEoJFp5lzQhN7craJP6Ed41mWYqVuoPId8
# AorRbrcWc+ZfwFSY1XS+wc3iEZGtIxg93eFyRJa0lV7Ae46ZeBZDE1ZXs6KzO7V3
# 3EByrKPrmzU+sQghoefEQzd5Mr6155wsTLxDKZmOMNOsIeDjHfrYBzN2VAAiKrlN
# IC5waNrlU/yDXNOd8v9EDERm8tLjvUYAGm0CuiVdjaExUd1URhxN25mW7xocBFym
# Fe944Hn+Xds+qkxV/ZoVqW/hpvvfcDDpw+5CRu3CkwWJ+n1jez/QcYF8AOiYrg54
# NMMl+68KnyBr3TsTjxKM4kEaSHpzoHdpx7Zcf4LIHv5YGygrqGytXm3ABdJ7t+uA
# /iU3/gKbaKxCXcPu9czc8FB10jZpnOZ7BN9uBmm23goJSFmH63sUYHpkqmlD75HH
# TOwY3WzvUy2MmeFe8nI+z1TIvWfspA9MRf/TuTAjB0yPEL+GltmZWrSZVxykzLsV
# iVO6LAUP5MSeGbEYNNVMnbrt9x+vJJUEeKgDu+6B5dpffItKoZB0JaezPkvILFa9
# x8jvOOJckvB595yEunQtYQEgfn7R8k8HWV+LLUNS60YMlOH1Zkd5d9VUWx+tJDfL
# RVpOoERIyNiwmcUVhAn21klJwGW45hpxbqCo8YLoRT5s1gLXCmeDBVrJpBAwggZZ
# MIIEQaADAgECAg0B7BySQN79LkBdfEd0MA0GCSqGSIb3DQEBDAUAMEwxIDAeBgNV
# BAsTF0dsb2JhbFNpZ24gUm9vdCBDQSAtIFI2MRMwEQYDVQQKEwpHbG9iYWxTaWdu
# MRMwEQYDVQQDEwpHbG9iYWxTaWduMB4XDTE4MDYyMDAwMDAwMFoXDTM0MTIxMDAw
# MDAwMFowWzELMAkGA1UEBhMCQkUxGTAXBgNVBAoTEEdsb2JhbFNpZ24gbnYtc2Ex
# MTAvBgNVBAMTKEdsb2JhbFNpZ24gVGltZXN0YW1waW5nIENBIC0gU0hBMzg0IC0g
# RzQwggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQDwAuIwI/rgG+GadLOv
# dYNfqUdSx2E6Y3w5I3ltdPwx5HQSGZb6zidiW64HiifuV6PENe2zNMeswwzrgGZt
# 0ShKwSy7uXDycq6M95laXXauv0SofEEkjo+6xU//NkGrpy39eE5DiP6TGRfZ7jHP
# vIo7bmrEiPDul/bc8xigS5kcDoenJuGIyaDlmeKe9JxMP11b7Lbv0mXPRQtUPbFU
# UweLmW64VJmKqDGSO/J6ffwOWN+BauGwbB5lgirUIceU/kKWO/ELsX9/RpgOhz16
# ZevRVqkuvftYPbWF+lOZTVt07XJLog2CNxkM0KvqWsHvD9WZuT/0TzXxnA/TNxNS
# 2SU07Zbv+GfqCL6PSXr/kLHU9ykV1/kNXdaHQx50xHAotIB7vSqbu4ThDqxvDbm1
# 9m1W/oodCT4kDmcmx/yyDaCUsLKUzHvmZ/6mWLLU2EESwVX9bpHFu7FMCEue1EIG
# bxsY1TbqZK7O/fUF5uJm0A4FIayxEQYjGeT7BTRE6giunUlnEYuC5a1ahqdm/TMD
# Ad6ZJflxbumcXQJMYDzPAo8B/XLukvGnEt5CEk3sqSbldwKsDlcMCdFhniaI/Miy
# Tdtk8EWfusE/VKPYdgKVbGqNyiJc9gwE4yn6S7Ac0zd0hNkdZqs0c48efXxeltY9
# GbCX6oxQkW2vV4Z+EDcdaxoU3wIDAQABo4IBKTCCASUwDgYDVR0PAQH/BAQDAgGG
# MBIGA1UdEwEB/wQIMAYBAf8CAQAwHQYDVR0OBBYEFOoWxmnn48tXRTkzpPBAvtDD
# vWWWMB8GA1UdIwQYMBaAFK5sBaOTE+Ki5+LXHNbH8H/IZ1OgMD4GCCsGAQUFBwEB
# BDIwMDAuBggrBgEFBQcwAYYiaHR0cDovL29jc3AyLmdsb2JhbHNpZ24uY29tL3Jv
# b3RyNjA2BgNVHR8ELzAtMCugKaAnhiVodHRwOi8vY3JsLmdsb2JhbHNpZ24uY29t
# L3Jvb3QtcjYuY3JsMEcGA1UdIARAMD4wPAYEVR0gADA0MDIGCCsGAQUFBwIBFiZo
# dHRwczovL3d3dy5nbG9iYWxzaWduLmNvbS9yZXBvc2l0b3J5LzANBgkqhkiG9w0B
# AQwFAAOCAgEAf+KI2VdnK0JfgacJC7rEuygYVtZMv9sbB3DG+wsJrQA6YDMfOcYW
# axlASSUIHuSb99akDY8elvKGohfeQb9P4byrze7AI4zGhf5LFST5GETsH8KkrNCy
# z+zCVmUdvX/23oLIt59h07VGSJiXAmd6FpVK22LG0LMCzDRIRVXd7OlKn14U7XIQ
# cXZw0g+W8+o3V5SRGK/cjZk4GVjCqaF+om4VJuq0+X8q5+dIZGkv0pqhcvb3JEt0
# Wn1yhjWzAlcfi5z8u6xM3vreU0yD/RKxtklVT3WdrG9KyC5qucqIwxIwTrIIc59e
# odaZzul9S5YszBZrGM3kWTeGCSziRdayzW6CdaXajR63Wy+ILj198fKRMAWcznt8
# oMWsr1EG8BHHHTDFUVZg6HyVPSLj1QokUyeXgPpIiScseeI85Zse46qEgok+wEr1
# If5iEO0dMPz2zOpIJ3yLdUJ/a8vzpWuVHwRYNAqJ7YJQ5NF7qMnmvkiqK1XZjbcl
# IA4bUaDUY6qD6mxyYUrJ+kPExlfFnbY8sIuwuRwx773vFNgUQGwgHcIt6AvGjW2M
# tnHtUiH+PvafnzkarqzSL3ogsfSsqh3iLRSd+pZqHcY8yvPZHL9TTaRHWXyVxENB
# +SXiLBB+gfkNlKd98rUJ9dhgckBQlSDUQ0S++qCV5yBZtnjGpGqqIpswggZoMIIE
# UKADAgECAhABSJA9woq8p6EZTQwcV7gpMA0GCSqGSIb3DQEBCwUAMFsxCzAJBgNV
# BAYTAkJFMRkwFwYDVQQKExBHbG9iYWxTaWduIG52LXNhMTEwLwYDVQQDEyhHbG9i
# YWxTaWduIFRpbWVzdGFtcGluZyBDQSAtIFNIQTM4NCAtIEc0MB4XDTIyMDQwNjA3
# NDE1OFoXDTMzMDUwODA3NDE1OFowYzELMAkGA1UEBhMCQkUxGTAXBgNVBAoMEEds
# b2JhbFNpZ24gbnYtc2ExOTA3BgNVBAMMMEdsb2JhbHNpZ24gVFNBIGZvciBNUyBB
# dXRoZW50aWNvZGUgQWR2YW5jZWQgLSBHNDCCAaIwDQYJKoZIhvcNAQEBBQADggGP
# ADCCAYoCggGBAMLJ3AO2G1D6Kg3onKQh2yinHfWAtRJ0I/5eL8MaXZayIBkZUF92
# IyY1xiHslO+1ojrFkIGbIe8LJ6TjF2Q72pPUVi8811j5bazAL5B4I0nA+MGPcBPU
# a98miFp2e0j34aSm7wsa8yVUD4CeIxISE9Gw9wLjKw3/QD4AQkPeGu9M9Iep8p48
# 0Abn4mPS60xb3V1YlNPlpTkoqgdediMw/Px/mA3FZW0b1XRFOkawohZ13qLCKnB8
# tna82Ruuul2c9oeVzqqo4rWjsZNuQKWbEIh2Fk40ofye8eEaVNHIJFeUdq3Cx+yj
# o5Z14sYoawIF6Eu5teBSK3gBjCoxLEzoBeVvnw+EJi5obPrLTRl8GMH/ahqpy76j
# dfjpyBiyzN0vQUAgHM+ICxfJsIpDy+Jrk1HxEb5CvPhR8toAAr4IGCgFJ8TcO113
# KR4Z1EEqZn20UnNcQqWQ043Fo6o3znMBlCQZQkPRlI9Lft3LbbwbTnv5qgsiS0mA
# SXAbLU/eNGA+vQIDAQABo4IBnjCCAZowDgYDVR0PAQH/BAQDAgeAMBYGA1UdJQEB
# /wQMMAoGCCsGAQUFBwMIMB0GA1UdDgQWBBRba3v0cHQIwQ0qyO/xxLlA0krG/TBM
# BgNVHSAERTBDMEEGCSsGAQQBoDIBHjA0MDIGCCsGAQUFBwIBFiZodHRwczovL3d3
# dy5nbG9iYWxzaWduLmNvbS9yZXBvc2l0b3J5LzAMBgNVHRMBAf8EAjAAMIGQBggr
# BgEFBQcBAQSBgzCBgDA5BggrBgEFBQcwAYYtaHR0cDovL29jc3AuZ2xvYmFsc2ln
# bi5jb20vY2EvZ3N0c2FjYXNoYTM4NGc0MEMGCCsGAQUFBzAChjdodHRwOi8vc2Vj
# dXJlLmdsb2JhbHNpZ24uY29tL2NhY2VydC9nc3RzYWNhc2hhMzg0ZzQuY3J0MB8G
# A1UdIwQYMBaAFOoWxmnn48tXRTkzpPBAvtDDvWWWMEEGA1UdHwQ6MDgwNqA0oDKG
# MGh0dHA6Ly9jcmwuZ2xvYmFsc2lnbi5jb20vY2EvZ3N0c2FjYXNoYTM4NGc0LmNy
# bDANBgkqhkiG9w0BAQsFAAOCAgEALms+j3+wsGDZ8Z2E3JW2318NvyRR4xoGqlUE
# y2HB72Vxrgv9lCRXAMfk9gy8GJV9LxlqYDOmvtAIVVYEtuP+HrvlEHZUO6tcIV4q
# NU1Gy6ZMugRAYGAs29P2nd7KMhAMeLC7VsUHS3C8pw+rcryNy+vuwUxr2fqYoXQ+
# 6ajIeXx2d0j9z+PwDcHpw5LgBwwTLz9rfzXZ1bfub3xYwPE/DBmyAqNJTJwEw/C0
# l6fgTWolujQWYmbIeLxpc6pfcqI1WB4m678yFKoSeuv0lmt/cqzqpzkIMwE2PmEk
# fhGdER52IlTjQLsuhgx2nmnSxBw9oguMiAQDVN7pGxf+LCue2dZbIjj8ZECGzRd/
# 4amfub+SQahvJmr0DyiwQJGQL062dlC8TSPZf09rkymnbOfQMD6pkx/CUCs5xbL4
# TSck0f122L75k/SpVArVdljRPJ7qGugkxPs28S9Z05LD7MtgUh4cRiUI/37Zk64U
# laiGigcuVItzTDcVOFBWh/FPrhyPyaFsLwv8uxxvLb2qtutoI/DtlCcUY8us9GeK
# LIHTFBIYAT+Eeq7sR2A/aFiZyUrCoZkVBcKt3qLv16dVfLyEG02Uu45KhUTZgT2q
# oyVVX6RrzTZsAPn/ct5a7P/JoEGWGkBqhZEcr3VjqMtaM7WUM36yjQ9zvof8rzpz
# H3sg23IwggbhMIIEyaADAgECAhEAh1/7vjU8naMhtEVWjN4JUDANBgkqhkiG9w0B
# AQwFADCBiDELMAkGA1UEBhMCVVMxEzARBgNVBAgTCk5ldyBKZXJzZXkxFDASBgNV
# BAcTC0plcnNleSBDaXR5MR4wHAYDVQQKExVUaGUgVVNFUlRSVVNUIE5ldHdvcmsx
# LjAsBgNVBAMTJVVTRVJUcnVzdCBSU0EgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkw
# HhcNMjAwMjE4MDAwMDAwWhcNMzMwNTAxMjM1OTU5WjBKMQswCQYDVQQGEwJOTDEZ
# MBcGA1UEChMQR0VBTlQgVmVyZW5pZ2luZzEgMB4GA1UEAxMXR0VBTlQgQ29kZSBT
# aWduaW5nIENBIDQwggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQDWi+0C
# OTTsm4TAqk1MCLIFJoeiMfMzLt1M0w92fkDz4dg7wJXY8stKXxqM6VN+LXZs4lea
# Eb7NNFgBxJ0KxGqac69GnYjPFTKtCkZ9oz1YkqlJPKbFCWlujnbEsR0we+AmCEL7
# 3jKJ/SoDh5AnexGbogDAHsS64Nefuh4VRHG77CcFYYWszhiXnNcyYO1EzYFzHvcs
# nkc1dCeXxByDLWooeMy9EJOyG5x0T8FylnE22S4J1Yw27su2VVZ30D9dLvZ5UmVS
# xe8eELuU2lnAeJ2P4O5yghXcNCW8MNXSUnDOTW1oZjU/GeJTskKiy8RcMSR9L/mT
# ObMRwxeVJ3Dn83+jLUsf+aIF8w+yyUeZ0tmadgnt5goCke4Ngt/2pNZXGLdgN/bY
# Sz+rAFqybX2JGq+fZsA7+PjzzqHmThKsbdwmDc6PZijFdfGbNOKlR+LxEQI1dfco
# SK3N5PQAlm8gNpVdqUvjOI3h1R/8Blzx1MFI/ZrJDUDZSvPfP7AHX8eywlUKsdeJ
# Ml40fk1JqFq1uJUoQTt3emgscxMVPt1ciH0ft/AOOS6Z27Dyho/f8V88gYytrXw+
# 0dYESvEZpp3kLLYw5L63AFbk/CJSK89nLe7CPPDlxGtqkAfJl3KrhCzO/rhPFfw+
# hkC9YWQLJhrwfKNh3q+tOAxV/jX0HZxkKYUWHQIDAQABo4IBgTCCAX0wHwYDVR0j
# BBgwFoAUU3m/WqorSs9UgOHYm8Cd8rIDZsswHQYDVR0OBBYEFJ8fomUPIU5jVUJy
# pGoZwSUBWVeUMA4GA1UdDwEB/wQEAwIBhjASBgNVHRMBAf8ECDAGAQH/AgEAMBMG
# A1UdJQQMMAoGCCsGAQUFBwMDMDgGA1UdIAQxMC8wLQYEVR0gADAlMCMGCCsGAQUF
# BwIBFhdodHRwczovL3NlY3RpZ28uY29tL0NQUzBQBgNVHR8ESTBHMEWgQ6BBhj9o
# dHRwOi8vY3JsLnVzZXJ0cnVzdC5jb20vVVNFUlRydXN0UlNBQ2VydGlmaWNhdGlv
# bkF1dGhvcml0eS5jcmwwdgYIKwYBBQUHAQEEajBoMD8GCCsGAQUFBzAChjNodHRw
# Oi8vY3J0LnVzZXJ0cnVzdC5jb20vVVNFUlRydXN0UlNBQWRkVHJ1c3RDQS5jcnQw
# JQYIKwYBBQUHMAGGGWh0dHA6Ly9vY3NwLnVzZXJ0cnVzdC5jb20wDQYJKoZIhvcN
# AQEMBQADggIBABGjLfn/GI9BgfCf4X5UrHZ9tdCu9wcWYUVhRzB7TokhcnOPXf/u
# RR7Qxrc0zrpbLRvK4HA7T/DhFTAEexL9NLz8P7iQghT+JgVvoTY8iha7+okM+oW4
# NFNXM4BrZQ69u0GEUC/B+aMoBBJ9rHIpAH+Iz6D2axg5YHoD28LxNYOsP6vlGCUB
# M7ECwBzOajS4fFtsuS3+Me4+/VXkYJgwV1B709+BopBAhcTynw8lksnoQ3EHCrwP
# FFD0Dm5pBL+tUxkVHiMc/UpOqWPqcpLSb01Gx6H3bZlC1wsQblf2j6XqpzdnBqeP
# SoYaw8mPHVZd/EaNVG79uvz8phfdR6pH122e3Jcd9k106Lpqos1I8CviXBHV7HZS
# o33dXVDN5je5AUyagdJGrMHPKgdpUxjU2lQGOMIJcNo+ruBnOQzTOikAruBfYQsm
# inK2gm5t60yoVLgx94CS9QF5/wToq7KXjUpGfKeHLw4e61GKXBSjlaOkAo/TGkD6
# 3kV7+AugmbzEhbKNbguj4CfSzsjbp0M3m8whGqd+Y0bkR9NC2m/143tq9mkYvijk
# ZPSD9Yc3dj9faGmdZGvtRKzOLI9GFj98SPp2lsJzc5GgIu36POiQ9CYU7x5OVj/y
# N8UN23NtgQ2otoHRMSYoD7w70LvhmXVw+jCU0EY/RBm3rS6IbjlFGFfQMIIHMzCC
# BRugAwIBAgIRAMgywLcOeysMnAspUzONPqMwDQYJKoZIhvcNAQEMBQAwSjELMAkG
# A1UEBhMCTkwxGTAXBgNVBAoTEEdFQU5UIFZlcmVuaWdpbmcxIDAeBgNVBAMTF0dF
# QU5UIENvZGUgU2lnbmluZyBDQSA0MB4XDTIzMDMyMTAwMDAwMFoXDTI0MDMyMDIz
# NTk1OVowga0xCzAJBgNVBAYTAkRFMRYwFAYDVQQIDA1OaWVkZXJzYWNoc2VuMUIw
# QAYDVQQKDDlHZXNlbGxzY2hhZnQgZsO8ciB3aXNzZW5zY2hhZnRsaWNoZSBEYXRl
# bnZlcmFyYmVpdHVuZyBtYkgxQjBABgNVBAMMOUdlc2VsbHNjaGFmdCBmw7xyIHdp
# c3NlbnNjaGFmdGxpY2hlIERhdGVudmVyYXJiZWl0dW5nIG1iSDCCAiIwDQYJKoZI
# hvcNAQEBBQADggIPADCCAgoCggIBANeVO8sL+TUlWovlV1rbmVKttbws4x7mqyR4
# fNydH7Cq9Af+61p8A4yzDOLi/AaONAW7G7k6LHGowilnn7cpKI63hSPwgy0xENCG
# DPaVPLrsGr3wqTCoqon+sTMnHkA1CO4FMxBmXN3EExFv9D4Y4ZEHU6yn9xtl5sFr
# M09GoD3C5uBebpUoTBlV/m1imICQETxdKBZnFbYetuTHDeeQMMYz2XRZhiOTpLI+
# 6wLRR1MQY8XCIh2MSNHcG2osdLqScMlu4k9z+esM9NCer5yDq/TNXLxG0x40UigE
# kHTWpClaz6hobMP6Yc5GAhCiYIsvayEQS3Dbw26CNMeJmGW4otkqG8DISuhUp822
# oGRpjc1vgWc62rwPEAZSL9Tk8pQxaEC9GjXJIL2QpSkox/kESHZENw0DFIMSGswz
# XMWkhs9HnTDWZUl/KTmsqJ6V2UbmJZrt1krCMhuNFP585Yh3iE3OSavISRkJPdH9
# lrMwBx1Z9UR0xcvybfQDHcJrVj0sUQAvvYjKbJXhEGYcBrdSHjo/evHs43WlfBuO
# QoMLVsJ148pJknwCHF35eMavHuDTa+VZL9BnPa6HkLN/0YDWRaxYtOvTeYRcX2oB
# X+x1AyJbNGCdamXQO92hxtN/s0XOShDTy//VzZ30oHuWqb++ssLxb/F0z2qx2VYm
# ChgZEEtfAgMBAAGjggGuMIIBqjAfBgNVHSMEGDAWgBSfH6JlDyFOY1VCcqRqGcEl
# AVlXlDAdBgNVHQ4EFgQU/9tBWy6B+ysoxEKOwTJmgA4uD5kwDgYDVR0PAQH/BAQD
# AgeAMAwGA1UdEwEB/wQCMAAwEwYDVR0lBAwwCgYIKwYBBQUHAwMwSQYDVR0gBEIw
# QDA0BgsrBgEEAbIxAQICTzAlMCMGCCsGAQUFBwIBFhdodHRwczovL3NlY3RpZ28u
# Y29tL0NQUzAIBgZngQwBBAEwRQYDVR0fBD4wPDA6oDigNoY0aHR0cDovL0dFQU5U
# LmNybC5zZWN0aWdvLmNvbS9HRUFOVENvZGVTaWduaW5nQ0E0LmNybDB7BggrBgEF
# BQcBAQRvMG0wQAYIKwYBBQUHMAKGNGh0dHA6Ly9HRUFOVC5jcnQuc2VjdGlnby5j
# b20vR0VBTlRDb2RlU2lnbmluZ0NBNC5jcnQwKQYIKwYBBQUHMAGGHWh0dHA6Ly9H
# RUFOVC5vY3NwLnNlY3RpZ28uY29tMCYGA1UdEQQfMB2BG3Rob3JzdGVuLmhpbmRl
# cm1hbm5AZ3dkZy5kZTANBgkqhkiG9w0BAQwFAAOCAgEAQ+Ft3yedXKTRyx9thFB7
# e3bGK2ecKzHDvyp7ZA8D043VY7bkoXjK/QysppeHx6JB/dDloatUlfssrQwjnZHG
# wp8Am7z0H+W/I6EI3fqOfxVa1NUIeDxuQ4ghx1eGINKvNWD+CRfwMR9CRxbmioj+
# 8PYiUdFv2TvHZtguMjsm9b+P5rUooYqB8VLoeIqn4uXSQO34K1hZuQG1AWXVvcQx
# 0TBe83kavuPGADECzrsgkj585exMa0+Xa/oKs68EEZQsUoSkbfQTibo79tsEHhGY
# 2Ef4lut+jY4a2oP+PWNlqesl4lxfPMwUGbSYP5uCCiIC6CVknH0SjL2hRyUcwBOc
# JRMTxNROLXoH5++Flx84ZLx8HBUOcyhgTNRgXfZU1xf4+Ccp/HAnXE8DZKVWAsCh
# 566+86l/GMc/ILnviyOmu8XwmjhlKh9qhvavE2HvPSpTKQSeMwVDkwI1igYqc+qv
# QnvFwWeOqP+XYg2fFEZrp/VXbmfDI9p7hL9CEz4cE+17LehsuWZepQb7cFzKO6Q+
# 5GHU40x3/ozEEobPxHBdtpc2Hnu6qKk7IHX00cvPmFA/QGrey/GqbUaJF5lTRDfr
# 0Mi/Q2+dJ8z9+epl0XaZBtCaRe1wTOvycdHGRk5/U+WxZmkoU5dCtrCZhKmn9puG
# t1Wi+Tg/XQLdPJ1Kd4BYzfYxggZIMIIGRAIBATBfMEoxCzAJBgNVBAYTAk5MMRkw
# FwYDVQQKExBHRUFOVCBWZXJlbmlnaW5nMSAwHgYDVQQDExdHRUFOVCBDb2RlIFNp
# Z25pbmcgQ0EgNAIRAMgywLcOeysMnAspUzONPqMwDQYJYIZIAWUDBAIBBQCgTDAZ
# BgkqhkiG9w0BCQMxDAYKKwYBBAGCNwIBBDAvBgkqhkiG9w0BCQQxIgQg5fw6dQ+R
# tqjpAgtqNMpzfKbTKsxFTzKnY2GRTK9TBNkwDQYJKoZIhvcNAQEBBQAEggIAjkXI
# wQVvSON19p4dF0MAwIQNZXWeNcwn9wO9wwUZchfqy7knm8oU+YRekZsM+W7M9ay2
# ZE8YzbbM9/9BDfeU/umXdoTtIPVfRx03a++hni2tGztcWcYAnbbIT0RwLpnPDzwJ
# wsgjFUaeYn0+/dpSkhwddt+qxbUR6ebJinO2wFBPgFbwdCbwVcC8h0/+GCt10Tdq
# HmwZiSxx/hlvplDAzN8TeVSwyLxBkqU6WAW8wSV2AuRERwJohnANJNGljsZ1V8nD
# xlhklmWjgFG7EfwX1D9i3dgPAGJDFpzrg2NMOMjZzC6aBM+h/2r/vR82mMcajHvD
# ccgsd5zYYtvHUpX9573cgu4WzD/U11IOZuMOzP2G46kYs67+5rKZNwIPeH6PQGaL
# 0Zl3DGgBcui0IG+ftuW1c37bARpxyYyH4dEOuXX7vTxN2ojR3pu9XCA5ugCnP0eq
# 4dG7TjGTsozasTbbjyJsEsEdy8Enk2w/MRkyeHEf58Iw9paPv6wTIk3bCwIEdv/Q
# qUucCl7Z3OIV3S++nZTrKHB0f7JWqDXhKMsxfjXJGKIOe7IK9SWK2PLhnhOVcV17
# e32Kc7wARXyum6gXwzFDTlYDRlqsh7OAtymGzstE8BRTCot0EulJPuN4E0nzGyl8
# g6MFKtz5YCnpZ4kirVxukdAQNEL/gab4cPzFSjKhggNsMIIDaAYJKoZIhvcNAQkG
# MYIDWTCCA1UCAQEwbzBbMQswCQYDVQQGEwJCRTEZMBcGA1UEChMQR2xvYmFsU2ln
# biBudi1zYTExMC8GA1UEAxMoR2xvYmFsU2lnbiBUaW1lc3RhbXBpbmcgQ0EgLSBT
# SEEzODQgLSBHNAIQAUiQPcKKvKehGU0MHFe4KTALBglghkgBZQMEAgGgggE9MBgG
# CSqGSIb3DQEJAzELBgkqhkiG9w0BBwEwHAYJKoZIhvcNAQkFMQ8XDTIzMDMzMTE0
# MDMzNFowKwYJKoZIhvcNAQk0MR4wHDALBglghkgBZQMEAgGhDQYJKoZIhvcNAQEL
# BQAwLwYJKoZIhvcNAQkEMSIEIDxn8ToYGQkhspKHCwfLb5yvWtf21sHEFx2eWWKj
# QqrlMIGkBgsqhkiG9w0BCRACDDGBlDCBkTCBjjCBiwQUMQMOF2qkWS6rLIut6DKZ
# /LVYXc8wczBfpF0wWzELMAkGA1UEBhMCQkUxGTAXBgNVBAoTEEdsb2JhbFNpZ24g
# bnYtc2ExMTAvBgNVBAMTKEdsb2JhbFNpZ24gVGltZXN0YW1waW5nIENBIC0gU0hB
# Mzg0IC0gRzQCEAFIkD3CirynoRlNDBxXuCkwDQYJKoZIhvcNAQELBQAEggGAYCfo
# mj1/qdf1ZwY2U0K5q015CSj9V4TSGS9UrTVuNI58zjBkRKlbzVFBoYwYWYLZOfaa
# MGxgpGZaoBP+C/jFIBuac6g+sbEMkiWsKk/RTMAGaK/QKOnUryXvCE7eRoT3fL2q
# HDM1zGVHm2aGr0NYk2/jIJVwKqIu2Gro0urmQla2EFRsv4f5dqR0qDx5Gs1wYC6n
# COdOu7JMyB4Bnxodh8W2F0wNf0qt7sUD1zfLi5hNfqd+5hiXy/DJf5WcoNFvZrRI
# jmXJADu3DooE8vcMArHzNYdHAcx+ycYbsKLcGrIXuQiAy1D36N8XcHhOFptzaW5e
# RPo5KXzLdDYUkM6kVHzUOKfhjss7zDd87Uj6BOI6C9CM+DpvOfqRyBW90C4/uySB
# NcK6aBxnDgmihT4yscvkU8KDJg1jeN5vE09ezqNhLn0dIdOb/n9EPIKySdtwlCt0
# 7pXRwrEs3Ckf+SxWAu5qzx7PmmZKwLxEhluFRZc0UM8FumXqUdqmKoUYl5RX
# SIG # End signature block
