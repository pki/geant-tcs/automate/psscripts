# PowerShell Skripte für die Automatisierung Géant TCS PKI mittels der Sectigo REST API
## Einleitung
Diese Skripte können als Beispiel dienen, wie mittels PowerShell Skripten die GÉANT TCS PKI REST API angesprochen werden kann für sich häufig wiederholenden Zertifikatsaufgaben eines Departments in einer Organisation in einem Mandanten der GÉANT TCS PKI.

## Abruf/Speicherort
In folgendem GWDG Nachrichtenartikel können die zu verwendenden Skripte für die drei am meisten genutzten oder vorkommenden Anwendungsfälle  nachgelesen werden:

> GWDG Nachrichten 4-5|22 - Das GÉANT TCS PKI-Backend – Skripte für Routineaufgaben der DRAOs

> https://www.gwdg.de/documents/20182/27257/GN_4-5-2022_www.pdf#page=10

Kontaktinformationen für eventuelle Rückfragen sind am Anfang des Artikels zu finden.

## Hinweis
Wichtig zu wissen ist, dass diese Skripte die Verwaltung der Organisationen/Departments von MPG, Universität Göttingen und GWDG abdecken. Für die eigene Einrichtung müssen diese Skripte eventuell noch angepasst werden um in die dortigen Abläufe hineinzupassen. Dies kann und wird sich in den Skripten in diesem Repository nicht wiederspiegeln. Diese Skripte sind auf einer "As is" Basis für interessierte DRAOs zur Verfügung gestellt. Für die konkrete Nutzung aber müssen diese auf die Abläude der Einrichtung, dass sich als Organisation/Department in der GÉANT TCS PKI befindet, angepasst werden.

# Informationen für spezielle Skripte
## UpdatePerson2ValidationType.ps1, GetDomainValidationStatus_ACMEAccount.ps1, GetDomainList.ps1, GetOrgList.ps1, ListAcmeAccount-2.ps1, ListPersons.ps1, GetClientAdminList.ps1, ClientCertReport.ps1
Diese Skripte nutzen, um zu funktionieren, ein spezielles Microsoft PowerShell Cmdlet, dass nicht im Standardumfang enthalten ist.
### Installation von Microsoft.PowerShell.ConsoleGuiTools 0.7.4
Wie unter dem URL https://www.powershellgallery.com/packages/Microsoft.PowerShell.ConsoleGuiTools/0.7.4 beschrieben das Paket installieren
Hier noch einmal der Installationsaufruf in der PowerShell Konsole:
```powershell
Install-Module -Name Microsoft.PowerShell.ConsoleGuiTools
``` 
Damit das installierte PowerShell Cmdlet immer zur Verfügung steht, muss noch das PowerShell Start-Skript geändert werden. Dazu das PowerShell Start-Skript wie folgt aurifen:
```powershell
<Mein_bevorzugter_Editor> $profile 
``` 
In das PowerShell Start-Skript Microsoft.PowerShell_profiles.ps1 die folgende Zeile einfügen:
```powershell
Import-Module -Name Microsoft.PowerShell.ConsoleGuiTools
``` 
Beispiel (m)eines PowerShell Start-Skriptes:

![Alt text](image-2.png)

Die Datei speichern und alle PowerShell-Instanzen schließen.
Eine PowerShell-Konsole aufrufen und zum Testen z.B. folgenden Befehl eingeben...

```powershell
Get-Module | Out-ConsoleGridView
```
...oder mit dem Alias `ocgv`
```powershell
Get-Module | ocgv
```
Das Ergebnis von diesem Testaufruf sieht wie folgt aus (Beispiel, kann variieren)
![Alt text](image.png)

Ab jetzt sollte die Ausführung des PowerShell-Skript `.\UpdatePerson2ValidationType.ps1` funktionieren.
### Beispiel
Der folgende Beispielhafte Aufruf...
```powershell
 psscripts  .\UpdatePerson2ValidationType.ps1  -setValidationTypeHigh

PowerShell credential request
Enter your credentials.
User: <IHR-DRAO-ACCOUNT>
Password for user <IHR-DRAO-ACCOUNT>: ************

Position: 0
Size: 10
Name: <Optional: Suche nachbestimmten Teil-Namen>
OrgID: <DEPARTMENT-Nummer>
E-Mail:
Alternative E-Mail:
Common Name:
 thinder   psscripts    master ≡  ?1 ~1   
```
...und die Auswahl der Person(en) durch drücken der Leertaste...
![Alt text](image-3.png) 
...liefert diese Beispiel-Ergebnis:
![Alt text](image-1.png)

Die Informationen können noch durch Eingabe eines Filter-Kriterium, z.B. `STANDARD` auf die Personen reduiziert werden, die noch nicht den `validationType` `HIGH` haben.
Mit der Leertaste vor jedem Eintrag die Personen auswählen, deren `validationType` auf `HIGH` gesetzt werden soll und abschließend mit der Return-Taste die Auswahl verlassen. Die ausgewählten Personen werden nun durch das restliche Skript verarbeitet.


